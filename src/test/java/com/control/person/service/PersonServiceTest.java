package com.control.person.service;

import com.control.person.configuration.Configuration;
import com.control.person.model.Person;
import com.control.person.persistence.PersonDAOImpl;
import com.control.person.response.PersonResponse;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PersonServiceTest {

    private static final int AGE_A = 20;
    private static final int AGE_B = 30;
    private static final int AGE_C = 40;
    private static final int LIFE_EXPECTATIVE = 72;
    private static final int DEATH_YEAR_A = 2071;
    private static final int DEATH_YEAR_B = 2061;
    private static final int DEATH_YEAR_C = 2051;

    private static final double AVERAGE_AGE = 30;

    @Mock
    private Configuration configuration;

    @Mock
    private PersonDAOImpl personDAO;

    @InjectMocks
    private PersonService personService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnAverageAge(){
        Mockito.when(personDAO.getAllPerson()).thenReturn(createPersonList());
        Assert.assertTrue(AVERAGE_AGE ==  personService.getAgeAverage().getAsDouble());
    }

    @Test
    public void shouldReturnDeathDate(){
        Mockito.when(personDAO.getAllPerson()).thenReturn(createPersonList());
        Mockito.when(configuration.getAgeAverage()).thenReturn(LIFE_EXPECTATIVE);
        List<PersonResponse> personResponseList = personService.getPersonList().get();
        Assert.assertEquals(DEATH_YEAR_A, personResponseList.get(0).getFechaDeMuerte().getYear());
        Assert.assertEquals(DEATH_YEAR_B, personResponseList.get(1).getFechaDeMuerte().getYear());
        Assert.assertEquals(DEATH_YEAR_C, personResponseList.get(2).getFechaDeMuerte().getYear());
    }

    private List<Person> createPersonList(){
        List<Person> personList = new ArrayList<>();
        personList.add(Person.builder()
                .fechaNacimiento(createDateWithYear(AGE_A))
                .build());
        personList.add(Person.builder()
                .fechaNacimiento(createDateWithYear(AGE_B))
                .build());
        personList.add(Person.builder()
                .fechaNacimiento(createDateWithYear(AGE_C))
                .build());
        return personList;
    }

    private Date createDateWithYear(int age){
        DateTime dateTime = DateTime.now();
        return dateTime.minusYears(age).toDate();
    }
}
