package com.control.person.service;

import com.control.person.configuration.Configuration;
import com.control.person.model.Person;
import com.control.person.persistence.PersonDAOImpl;
import com.control.person.request.PersonRequest;
import com.control.person.response.PersonResponse;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

@Service
public class PersonService {

    @Autowired
    private Configuration configuration;

    @Autowired
    private PersonDAOImpl personDAO;

    public String insertPerson(final PersonRequest personRequest) {
        Person person = Person.builder()
                .nombre(personRequest.getNombre())
                .apellido(personRequest.getApellido())
                .fechaNacimiento(personRequest.getFechaNacimiento().toDate())
                .build();
        return personDAO.insertPerson(person);
    }

    public OptionalDouble getAgeAverage() {
        return personDAO.getAllPerson()
                .stream()
                .mapToInt(a -> getAgeByBirthDate(a.getFechaNacimiento()))
                .average();
    }

    public Optional<List<PersonResponse>> getPersonList() {
        List<Person> personList = personDAO.getAllPerson();
        return Optional.of(personList
                .stream()
                .map(p -> PersonResponse.builder()
                        .nombre(p.getNombre())
                        .apellido(p.getApellido())
                        .fechaDeMuerte(calculateDeathDate(p.getFechaNacimiento()))
                        .build())
                .collect(Collectors.toList()));
    }

    private int getAgeByBirthDate(final Date birthDate) {
        DateTime currentDateTime = DateTime.now();
        return currentDateTime.get(DateTimeFieldType.year()) -
                new DateTime(birthDate).get(DateTimeFieldType.year());
    }

    private DateTime calculateDeathDate(final Date birthDate) {
        DateTime birthDateDateTime  = new DateTime(birthDate);
        return birthDateDateTime.plusYears(configuration.getAgeAverage());
    }
}
