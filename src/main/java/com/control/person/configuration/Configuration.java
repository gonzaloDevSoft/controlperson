package com.control.person.configuration;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class Configuration {

    @Value("${application.control.person.ageAverage}")
    private int ageAverage;
}
