package com.control.person;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonControlApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonControlApplication.class, args);
	}

}
