package com.control.person.request;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class PersonRequest{
    private String nombre;
    private String apellido;
    private DateTime fechaNacimiento;
}
