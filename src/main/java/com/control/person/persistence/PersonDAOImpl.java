package com.control.person.persistence;

import com.control.person.model.Person;
import com.control.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PersonDAOImpl{

    @Autowired
    private PersonRepository personRepository;

    public String insertPerson(final Person person){
        personRepository.save(person);
        return "PERSON SAVED";
    }

    public List<Person> getAllPerson(){
        return personRepository.findAll();
    }
}
