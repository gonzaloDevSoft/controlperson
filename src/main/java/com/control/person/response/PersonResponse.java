package com.control.person.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
@Builder
public class PersonResponse{
    private String nombre;
    private String apellido;
    private DateTime fechaDeMuerte;
}
