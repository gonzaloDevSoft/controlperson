package com.control.person.controller;

import com.control.person.request.PersonRequest;
import com.control.person.response.PersonResponse;
import com.control.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ControlPersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/person")
    public ResponseEntity createPerson(@RequestBody PersonRequest personRequest){
        try{
            String message = personService.insertPerson(personRequest);
            return ResponseEntity
                    .ok()
                    .body(message);
        }catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }

    @GetMapping("/persons")
    public ResponseEntity<List<PersonResponse>> obtainsListPerson(){
        try{
            List<PersonResponse> personList = personService.getPersonList().get();
            return ResponseEntity
                    .ok()
                    .body(personList);
        }catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }

    @GetMapping("/person/ageAverage")
    public ResponseEntity obtainsAgeAverage(){
        try{
            double ageAverage = personService.getAgeAverage().getAsDouble();
            return ResponseEntity
                    .ok()
                    .body(ageAverage);
        }catch (Exception ex){
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }
    }


}
